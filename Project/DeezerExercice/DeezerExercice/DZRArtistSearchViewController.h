//
//  DZRArtistSearchViewController.h
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeezerExercice-Swift.h"

@interface DZRArtistSearchViewController : UIViewController
@property (strong, nonatomic) DZRSearchArtistsViewModel *viewModel;

@end
