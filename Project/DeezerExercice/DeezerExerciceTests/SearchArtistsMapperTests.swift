//
//  SearchArtistsMapperTests.swift
//  DeezerExerciceTests
//
//  Created by Malek BARKAOUI on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import XCTest
@testable import DeezerExercice


class SearchArtistsMapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testMapArtists() throws {
        // This is an example of a performance test case.
        let searchArtistMapper = DZRSearchArtistsMapper()
        let artistViewModelList = searchArtistMapper.map(restArtists: [
            RestArtist.mock(id: 1, name: "artist1", picture: "picture1", trackList: "trackList1"),
            RestArtist.mock(id: 2, name: "artist2", picture: "picture2", trackList: "trackList2"),
            RestArtist.mock(id: 3, name: "artist3", picture: "picture3", trackList: "trackList3")
        ])
        
        XCTAssertEqual(artistViewModelList.count, 3)
        XCTAssertEqual(artistViewModelList.first?.artistName, "artist1")
        XCTAssertEqual(artistViewModelList.first?.artistImage, "picture1")
        XCTAssertEqual(artistViewModelList.first?.trackList, "trackList1")
        
        XCTAssertEqual(artistViewModelList[1].artistName, "artist2")
        XCTAssertEqual(artistViewModelList[1].artistImage, "picture2")
        XCTAssertEqual(artistViewModelList[1].trackList, "trackList2")
        
        XCTAssertEqual(artistViewModelList[2].artistName, "artist3")
        XCTAssertEqual(artistViewModelList[2].artistImage, "picture3")
        XCTAssertEqual(artistViewModelList[2].trackList, "trackList3")
        
    }
}
