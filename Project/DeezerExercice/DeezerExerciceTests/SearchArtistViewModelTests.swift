//
//  SearchArtistViewModelTests.swift
//  DeezerExerciceTests
//
//  Created by Malek BARKAOUI on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import XCTest

@testable import DeezerExercice

class SearchArtistViewModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testWhenArtistsAreFetched() {
        //Given
        let searchArtistDatalogicMock = MockSearchArtistDatalogic.mock(result: .success([ArtistViewModel(artistName: "artiste1", artistImage: "image1", trackList: "trackList1"), ArtistViewModel(artistName: "artiste2", artistImage: "image2", trackList: "trackList2"), ArtistViewModel(artistName: "artiste1", artistImage: "image2", trackList: "trackList3")]))
        let viewModel = DZRSearchArtistsViewModel(searchArtistsDataLogic: searchArtistDatalogicMock)

        let expectation = self.expectation(description: "artistsFetched")


        viewModel.onArtistsListFetched = {
            //Then
            XCTAssertEqual(viewModel.artistListViewModels?.count, 3)
            expectation.fulfill()
        }

        // When
        viewModel.getArtists(artistName: "")

        waitForExpectations(timeout: 1, handler: nil)


    }

    func testWhenArtistSelected() {
        //Given
        let searchArtistDatalogicMock = MockSearchArtistDatalogic.mock(result: .success([ArtistViewModel(artistName: "artiste1", artistImage: "image1", trackList: "trackList1"), ArtistViewModel(artistName: "artiste2", artistImage: "image2", trackList: "trackList2"), ArtistViewModel(artistName: "artiste1", artistImage: "image2", trackList: "trackList3")]))
        let viewModel = DZRSearchArtistsViewModel(searchArtistsDataLogic: searchArtistDatalogicMock)

        let expectation = self.expectation(description: "artistSelected")


        viewModel.onArtistSelected = { artistViewModel in
            //Then
            XCTAssertEqual(artistViewModel.artistName, "artiste1")
            XCTAssertEqual(artistViewModel.artistImage, "image1")
            XCTAssertEqual(artistViewModel.trackList, "trackList1")

            expectation.fulfill()
        }

        // When
        viewModel.getArtists(artistName: "")
        viewModel.didSelectArtist(index: 0)

        waitForExpectations(timeout: 1, handler: nil)

    }

    func testWhenArtistsFetchReturnError() {
        //Given
        let searchArtistDatalogicMock = MockSearchArtistDatalogic.mock(result: .failure(NSError(domain: "failed", code: 500, userInfo: nil)))
        let viewModel = DZRSearchArtistsViewModel(searchArtistsDataLogic: searchArtistDatalogicMock)

        let expectation = self.expectation(description: "artistsFetchedError")


        viewModel.onError = { error  in
            //Then
            XCTAssertEqual(error.code, 500)
            expectation.fulfill()
        }

        // When
        viewModel.getArtists(artistName: "")

        waitForExpectations(timeout: 1, handler: nil)

    }

}
