//
//  DZRSearchArtistsViewModel.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc final class DZRSearchArtistsViewModel: NSObject {

    private let searchArtistsDataLogic: DZRSearchArtistsDataLogicProtocol
    var artistListViewModels: [ArtistViewModel]?

    @objc var onArtistsListFetched: (() -> Void)?
    @objc var onArtistSelected: ((ArtistViewModel) -> Void)?
    @objc var onError: ((NSError) -> Void)?

    public init(searchArtistsDataLogic: DZRSearchArtistsDataLogicProtocol = DZRSearchArtistsDataLogic()) {
        self.searchArtistsDataLogic = searchArtistsDataLogic
    }

    @objc func artistListCount() -> Int {
        artistListViewModels?.count ?? 0
    }

    @objc func didSelectArtist(index: Int) {
       let artistViewModel = artistFor(row: index)
        onArtistSelected?(artistViewModel)
    }

    @objc func getArtists(artistName: String) {
        searchArtistsDataLogic.getArtists(artistName: artistName) { response in
            switch response {
            case .success(let artistList):
                self.artistListViewModels = artistList
                self.onArtistsListFetched?()
            case .failure(let error):
                self.onError?(error as NSError)
            }
        }
    }

    @objc func artistFor(row: Int) -> ArtistViewModel {
        return self.artistListViewModels?[row] ?? ArtistViewModel(artistName: "", artistImage: "", trackList: "")
    }
}

extension DZRSearchArtistsViewModel {
   @objc static func builder() -> DZRSearchArtistsViewModel {
        return DZRSearchArtistsViewModel()
    }
}
