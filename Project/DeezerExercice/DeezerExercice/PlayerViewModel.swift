//
//  PlayerViewModel.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 09/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class PlayerViewModel {
    let albumViewModel: AlbumViewModel
    var index: Int

    var onViewDidLoad: ((TrackViewModel) -> Void)?
    var playPauseTrack: ((Bool) -> Void)?
    var playNextTrack: ((TrackViewModel) -> Void)?
    var playPreviousTrack: ((TrackViewModel) -> Void)?
    var trackDidplaytoEnd: (() -> Void)?
    var updateSliderLevel: ((Double, Double) -> Void)?

    var audioPlayer = AudioReader.default

    init(albumViewModel: AlbumViewModel, index: Int) {
        self.albumViewModel = albumViewModel
        self.index = index
        bindAudioReader()
    }

    private func bindAudioReader() {
        audioPlayer.endTrackReached = {
            self.audioPlayer.player?.replaceCurrentItem(with: nil)
            self.trackDidplaytoEnd?()
        }

        audioPlayer.updateTrackTime = { time, duration in
            self.updateSliderLevel?(time, duration)
        }
    }


    func viewDidLoad() {
        onViewDidLoad?(self.albumViewModel.albumTracks[index])
    }

    func didTapPlayPause() {
        if audioPlayer.playerIsPlaying(){
            audioPlayer.playPauseAudio()
            playPauseTrack?(true)
        } else {
            audioPlayer.playPauseAudio(track: albumViewModel.albumTracks[index].trackPreview)
            playPauseTrack?(false)
        }
    }

    func didTapNextTrack() {
        if index < albumViewModel.albumTracks.count - 1 {
            index += 1
            audioPlayer.playNextPreviousTrack(track: albumViewModel.albumTracks[index].trackPreview)
        }

        playPreviousTrack?(albumViewModel.albumTracks[index])
    }

    func didTapBackTrack() {
        if index > 0 {
            index -= 1
            audioPlayer.playNextPreviousTrack(track: albumViewModel.albumTracks[index].trackPreview)
        }

        playPreviousTrack?(albumViewModel.albumTracks[index])
    }

    func getAlbumName() -> String {
        albumViewModel.albumImage
    }
}
