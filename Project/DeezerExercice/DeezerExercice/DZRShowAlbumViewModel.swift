//
//  DZRShowAlbumViewModel.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class DZRShowAlbumViewModel {
    private let showAlbumDataLogic: DZRShowAlbumDataLogicProtocol
    private let trackList: String

    var albumViewModel: AlbumViewModel?

    var onAlbumFetched: ((AlbumViewModel?) -> Void)?
    var onAlbumNotAvailable: (() -> Void)?
    var onDidSelectTrack: ((Int) -> Void)?
    var onError: ((Error) -> Void)?

    init(trackList: String, showAlbumDataLogic: DZRShowAlbumDataLogicProtocol = DZRShowAlbumDataLogic()) {
        self.trackList = trackList
        self.showAlbumDataLogic = showAlbumDataLogic
    }

    func viewDidLoad() {
        showAlbumDataLogic.getAlbums(urlString: trackList) { response in
            switch response {
            case .success(let albumViewModel):
                if albumViewModel == nil {
                    self.onAlbumNotAvailable?()
                } else {
                    self.albumViewModel = albumViewModel
                    self.onAlbumFetched?(albumViewModel)
                }

            case .failure(let error):
                self.onError?(error)
            }
        }
    }


    func didSelectTrack(at index: Int) {
        onDidSelectTrack?(index)
    }
}
