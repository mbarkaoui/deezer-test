//
//  DZRTrackTableViewCell.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 04/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import UIKit
import AVFoundation
import AVFAudio

class DZRTrackTableViewCell: UITableViewCell {
    let shadowContainer = UIView()
    let containerView = UIView()
    let horizontalStackView = UIStackView()
    let verticalStackView = UIStackView()
    let trackNameLabel = UILabel()
    let trackImageView = UIImageView()

    var viewModel: TrackViewModel?

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    override func layoutSubviews() {
        DispatchQueue.main.async {
            self.shadowContainer.addShadow(ShadowProperties(cornerRadius: 4, opacity: 1, offset: CGSize(width: 0, height: 0), radius: 10, color: UIColor(red: 0.302, green: 0.302, blue: 0.302, alpha: 0.2)))
        }
    }

    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        //translatesAutoresizingMaskIntoConstraints = false
        setupHorizontalStackView()
    }

    private func setupHorizontalStackView() {
        contentView.addSubview(shadowContainer)
        containerView.backgroundColor = .white
        shadowContainer.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        shadowContainer.addSubview(containerView)
        containerView.anchor(top: shadowContainer.topAnchor, left: shadowContainer.leftAnchor, bottom: shadowContainer.bottomAnchor, right: shadowContainer.rightAnchor, paddingTop: 3, paddingLeft: 3, paddingBottom: 3, paddingRight: 3, width: 0, height: 0, enableInsets: false)
        containerView.layer.cornerRadius = 10

        // border
        containerView.layer.borderWidth = 0.6
        containerView.layer.borderColor = UIColor.black.cgColor


        containerView.addSubview(trackImageView)

        trackImageView.layer.cornerRadius = 10
        trackImageView.clipsToBounds = true
        trackImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        trackImageView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 0, width: 50, height: 50, enableInsets: false)
        trackNameLabel.numberOfLines = 0
        containerView.addSubview(trackNameLabel)

        trackNameLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true

        trackNameLabel.anchor(top: nil, left: trackImageView.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
    }


    func configure(trackViewModel: TrackViewModel) {
        selectionStyle = .none
        viewModel = trackViewModel
        trackImageView.load(url: URL(string: "https://cdns-images.dzcdn.net/images/artist/\(trackViewModel.trackImage)/56x56-000000-80-0-0.jpg") ?? URL(fileURLWithPath: ""))
        trackNameLabel.text = trackViewModel.trackName
    }

}
