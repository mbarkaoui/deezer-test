//
//  ArtistsResponse.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public struct ArtistsResponse {
    let artists: [RestArtist]
    let total: Int
    let next: String

    public init(artists: [RestArtist], total: Int, next: String){
        self.artists = artists
        self.total = total
        self.next = next
    }
}

extension ArtistsResponse: Decodable {

    private enum ArtistsResponseKeys: String, CodingKey {
        case artists = "data"
        case total
        case next
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ArtistsResponseKeys.self)
        artists = try container.decode([RestArtist].self, forKey: .artists)
        total = try container.decode(Int.self, forKey: .total)
        next = try container.decode(String.self, forKey: .next)
    }
}
