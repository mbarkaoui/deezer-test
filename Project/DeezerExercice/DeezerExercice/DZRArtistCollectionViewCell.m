//
//  DZRArtistCollectionViewCell.m
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import "DZRArtistCollectionViewCell.h"
#import "DeezerExercice-Swift.h"


@implementation DZRArtistCollectionViewCell


- (void)awakeFromNib {
    _artistImage.layer.cornerRadius = _artistImage.frame.size.height / 2;
    _artistImage.clipsToBounds = TRUE;

    [super awakeFromNib];
}

-(void)configure:(ArtistViewModel *)viewModel {
    [_artistImage loadWithUrl:[NSURL URLWithString: viewModel.artistImage]];
    _artistName.text = viewModel.artistName;
}

@end
