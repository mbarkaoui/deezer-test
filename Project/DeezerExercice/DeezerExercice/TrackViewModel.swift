//
//  TrackViewModel.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class TrackViewModel {
    let trackName: String
    let trackPreview: String
    let trackImage: String

    init(trackName: String, trackPreview: String, trackImage: String) {
        self.trackName = trackName
        self.trackPreview = trackPreview
        self.trackImage = trackImage
    }
}
