//
//  AlbumsResponse.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public struct AlbumsResponse {
    let albums: [ArtistAlbum]?
    let total: Int
}

extension AlbumsResponse: Decodable {

    private enum AlbumsResponseKeys: String, CodingKey {
        case albums = "data"
        case total
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: AlbumsResponseKeys.self)
        albums = try container.decode([ArtistAlbum]?.self, forKey: .albums)
        total = try container.decode(Int.self, forKey: .total)
    }
}
