//
//  ArtistsEndPoint.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public enum ArtistApi {
    case artists(String)
}

extension ArtistApi: EndPointType {
    var baseURL: String {
        let url = environmentBaseURL
        return url
    }


    var environmentBaseURL: String {
        switch NetworkManager.environment {
        case .production: return "https://api.deezer.com"
        case .qa: return "https://api.deezer.com"
        case .staging: return "https://api.deezer.com"
        }
    }

    var path: String {
        switch self {
        case .artists:
            return "/search/artist"
        }
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var task: HTTPTask {
        switch self {
        case .artists(let name):

            return .requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["q" : name])

        }
    }

    var headers: HTTPHeaders? {
        return nil
    }
}
