//
//  ArtistAPIMock.swift
//  DeezerExerciceTests
//
//  Created by Malek BARKAOUI on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import DeezerExercice

final class ArtistAPIMock: ArtistsAPIType {

    var result: Result<ArtistsResponse, Error>

    init() {
        result = .success(ArtistsResponse(artists: [], total: 0, next: ""))
    }

    func getArtists(name: String, completion: @escaping (Result<ArtistsResponse, Error>) -> ()) {
        completion(result)
    }
}

extension ArtistAPIMock {
    static func mock(result: Result<ArtistsResponse, Error>) -> ArtistsAPIType {
        let artistApiMock = ArtistAPIMock()
        artistApiMock.result = result
        return artistApiMock
    }
}
