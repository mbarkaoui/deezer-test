//
//  Runner.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 04/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public struct Runner {

    /**
     Runs the code block if we're in the main thread
     Otherwise the block is queued up to run after the current runloop completes (to be executed on the main queue)
     */
    public static func runOnMainThread(_ work: @escaping () -> Void) {
        if Thread.isMainThread {
            work()
        } else {
            DispatchQueue.main.async {
                work()
            }
        }
    }
}
