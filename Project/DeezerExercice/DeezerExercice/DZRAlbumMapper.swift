//
//  DZRAlbumMapper.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol DZRAlbumMapperType {
    func map(album: ArtistAlbum) -> AlbumViewModel
}

final class DZRAlbumMapper: DZRAlbumMapperType {
    func map(album: ArtistAlbum) -> AlbumViewModel {
        return AlbumViewModel(albumName: album.title ?? "", albumImage: album.album?.cover ?? "", trackList: album.album?.tracklist ?? "")
    }
}


