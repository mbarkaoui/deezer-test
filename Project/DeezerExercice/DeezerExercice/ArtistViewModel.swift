//
//  ArtistViewModel.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc public class ArtistViewModel: NSObject {
    
    @objc let artistName: String
    @objc let artistImage: String
    @objc let trackList: String
    
    public init(artistName: String, artistImage: String, trackList: String) {
        self.artistName = artistName
        self.artistImage = artistImage
        self.trackList = trackList
    }
}
