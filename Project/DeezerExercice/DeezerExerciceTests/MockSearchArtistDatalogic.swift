//
//  MockSearchArtistDatalogic.swift
//  DeezerExerciceTests
//
//  Created by Malek BARKAOUI on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import DeezerExercice

final class MockSearchArtistDatalogic: DZRSearchArtistsDataLogicProtocol {

    var result: Result<[ArtistViewModel], Error>

    init() {
        result = .success([])
    }

    func getArtists(artistName: String, completion: @escaping (Result<[ArtistViewModel], Error>) -> ()) {
        completion(result)
    }
}

extension MockSearchArtistDatalogic {
    static func mock(result: Result<[ArtistViewModel], Error>) -> DZRSearchArtistsDataLogicProtocol {
        let mockSearchArtistDatalogic = MockSearchArtistDatalogic()
        mockSearchArtistDatalogic.result = result
        return mockSearchArtistDatalogic
    }
}

