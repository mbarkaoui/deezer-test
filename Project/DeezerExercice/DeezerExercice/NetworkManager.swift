//
//  NetworkManager.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

enum ResultType<String>{
case success
case failure(String)
}

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

class NetworkManager {
    static let environment : NetworkEnvironment = .production
    static let APIKey = "GL6rpQa23Cfkgmci1aaqQLRddAnnouvN"

}
