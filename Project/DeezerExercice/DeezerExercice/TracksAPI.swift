//
//  TracksAPI.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public protocol TracksAPIType {
    func getTracks(urlString: String, completion: @escaping (_ result: Result<TracksResponse, Error>)->())
}

final class TracksAPI: TracksAPIType {

    let tracksRouter = Router<TracksApi>()

    public func getTracks(urlString: String, completion: @escaping (_ result:Result<TracksResponse, Error>)->()){
        tracksRouter.request(.tracks(urlString)) { (result: Result<TracksResponse, Error>) in
            completion(result)
        }
    }
}
