# Deezer Test

to test the app the better is to run it on a real device because there's a bug for simulator with avPlayer

## Architecture
I realized this project based on the mvvm architecture with binding viewModel & View with completionBlock to be declarative to the maximum.

## Data
for the data part I use the service(to call API) ,mapper(to map from rest models to project models) and the datalogic (called by viewModel to return exact data format we need)

## UI
i've made UI with constraints programatically to avoid future conflicts on storyBoard and to gain time and to have more control & understanding what're we doing exactly.

## Unit tests
i mocked datalogic in order to test the viewModel with injecting it to viewModel
i mocked service to test datalogic with injecting it to datalogic
i test artistmapper

## what i could improve if i have more time
# Test
test the playerViewModel (i've already prepare the reader to be testable with doing every traitement in function i've just to declare them in protocol to be able to mock the reader and inject it to the player)

test other scenario

# Feature


1. Managing better the errors with creating enum of String, error and send it directly to facilitate the handling in UI Part
2. Managing better the Colors and Font, Images with declaring in static function in a struct where ce can declare all fonts , colors, Images
3. Adding music to favorite or add it to playlist (with having a firebase storage) and we can list all favorite track in Favorite rubrique (tabBar)
4. To structure the navigation flow i could do a coordinator + router to separate the context
5. Better design for player with cooler animations
6. playing music in background when app is not on foreground
7. the possibility to see the played music in all screen of the app
8. adding localizable texts to manage multiple language in the app
9. change the rate of the sound , mute the sound , change volume , adding equalizer etc..

# Log
add a logging system for non fatal errors










