//
//  DZRShowAlbumViewController.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import UIKit

class DZRShowAlbumViewController: UIViewController {

    private let viewModel: DZRShowAlbumViewModel
    private let tableView = UITableView(frame: .zero, style: .plain)
    var safeArea: UILayoutGuide!

    @objc required init(trackList: String) {
        self.viewModel = DZRShowAlbumViewModel(trackList: trackList)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        bind()
        viewModel.viewDidLoad()
        setupTableView()
    }

    private func setupTableView() {
        tableView.register(DZRTrackTableViewCell.self, forCellReuseIdentifier: "DZRTrackTableViewCell")

        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.dataSource = self
        tableView.delegate = self
    }

    private func bind() {
        viewModel.onAlbumNotAvailable = { [weak self]  in
            Runner.runOnMainThread {
                self?.presentAlert(alertMessage: "no album available for this artist", completion: {
                    self?.dismissAlert()
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        }

        viewModel.onAlbumFetched = { [weak self] albumViewModel in
            Runner.runOnMainThread {
                self?.title = albumViewModel?.albumName
                self?.tableView.reloadData()
            }
        }

        viewModel.onDidSelectTrack = { [weak self] index in
            Runner.runOnMainThread {
                let playerViewModel = PlayerViewModel(albumViewModel: self?.viewModel.albumViewModel ?? AlbumViewModel(albumName: "", albumImage: "", trackList: ""), index: index)
                let playerViewController = PlayerViewController.builder(viewModel: playerViewModel)

                self?.present(playerViewController, animated: true, completion: nil)
            }
        }

        viewModel.onError = { error in
            Runner.runOnMainThread {
                self.presentAlert(alertMessage: "oups something went wrong") {
                    self.dismissAlert()
                }
            }
        }
    }
}

extension DZRShowAlbumViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.albumViewModel?.albumTracks.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "DZRTrackTableViewCell", for: indexPath) as! DZRTrackTableViewCell
        guard let tracksViewModel = viewModel.albumViewModel?.albumTracks[indexPath.row] else { return UITableViewCell() }
        cell.configure(trackViewModel: tracksViewModel)
        return cell
    }
}

extension DZRShowAlbumViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectTrack(at: indexPath.row)
    }

}

extension DZRShowAlbumViewController {
    @objc static func builder(trackList: String) -> DZRShowAlbumViewController {
        return DZRShowAlbumViewController(trackList: trackList)
    }
}
