//
//  AudioReader.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 04/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import AVFoundation

protocol AudioReaderDelegate: AnyObject {
    func didUpdateTrackTime()
}

protocol AudioReaderProtocol {
    
}

class AudioReader {

    static var `default`: AudioReader {
        AudioReader()
    }
    
    var player: AVPlayer? = AVPlayer()
    var timeObserverToken: Any?

    weak var delegate: AudioReaderDelegate?
    var updateTrackTime: ((Double,Double) -> Void)?
    var endTrackReached: (() -> Void)?


    init() {
        player?.actionAtItemEnd = .none
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: player?.currentItem)
    }


    @objc func playerItemDidReachEnd(notification: Notification) {
        endTrackReached?()
    }

    func pause() {
        player?.pause()
    }

    func isCurrentItemNil() -> Bool {
        player?.currentItem != nil
    }

    func playerIsPaused() -> Bool {
        player?.timeControlStatus == .paused
    }

    func playerIsPlaying() -> Bool {
        player?.timeControlStatus == .playing
    }

    func playNextPreviousTrack(track: String) {
        stop()
        playPauseAudio(track: track)
    }

    func play() {
        self.player?.volume = 1.0
        self.player?.play()
    }

    func playWithTrack(track: String){
        let url = NSURL(string: track)

        let playerItem = AVPlayerItem(url: url! as URL )

        self.player = AVPlayer(playerItem:playerItem)
        self.player?.volume = 1.0
        self.player?.play()
    }

    func addPeriodicTimeObserver() {
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 1, preferredTimescale: timeScale)

        self.timeObserverToken =  player?.addPeriodicTimeObserver(forInterval: time,
                                                                  queue: .main) { time in

            self.updateTrackTime?(time.seconds, self.player?.currentItem?.duration.seconds ?? 0)
        }
    }

    func playPauseAudio(track: String = "") {
        if playerIsPlaying() {
            pause()
            return
        } else {
            if playerIsPaused() && isCurrentItemNil() {
                play()
            } else {
               playWithTrack(track: track)
            }
        }

        addPeriodicTimeObserver()
    }

     func stop() {
        player?.replaceCurrentItem(with: nil)
        player = nil
    }
}
