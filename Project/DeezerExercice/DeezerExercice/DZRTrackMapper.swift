//
//  DZRTrackMapper.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol DZRTrackMapperType {
    func map(restTracks: [RestTrack]) -> [TrackViewModel]
}

final class DZRTrackMapper: DZRTrackMapperType {
    func map(restTracks: [RestTrack]) -> [TrackViewModel] {
        return restTracks.map(map)
    }

    private func map(restTrack: RestTrack) -> TrackViewModel {
        return TrackViewModel(trackName: restTrack.title ?? "", trackPreview: restTrack.preview ?? "", trackImage: restTrack.md5Image ?? "")
    }
}

