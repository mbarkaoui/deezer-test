//
//  UIViewExtensions.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 04/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import UIKit

public struct ShadowProperties {
  public var cornerRadius: CGFloat
  public var opacity: Float
  public var offset: CGSize
  public var radius: CGFloat
  public var color: UIColor?

  public init(cornerRadius: CGFloat, opacity: Float, offset: CGSize, radius: CGFloat, color: UIColor?) {
    self.cornerRadius = cornerRadius
    self.opacity = opacity
    self.offset = offset
    self.radius = radius
    self.color = color
  }
}

extension UIViewController {
    private static var alert = UIAlertController()
    @objc func presentAlert(alertMessage: String, completion: @escaping (() -> Void)) {
        UIViewController.alert = UIAlertController(title: "Warning", message: alertMessage, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            completion()
        })
        UIViewController.alert.addAction(okButton)
        present(UIViewController.alert, animated: true, completion: nil)
    }
    @objc func dismissAlert() {
        UIViewController.alert.dismiss(animated: true, completion: nil)
    }
}


extension UIView {
    public func addShadow(_ shadowProperties: ShadowProperties) {
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: shadowProperties.cornerRadius)

        layer.shadowPath = shadowPath.cgPath
        layer.shadowColor = shadowProperties.color?.cgColor
        layer.shadowOpacity = shadowProperties.opacity
        layer.shadowRadius = shadowProperties.radius
        layer.shadowOffset = shadowProperties.offset
        layer.bounds = bounds
      }

    public func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat, enableInsets: Bool) {
        var topInset = CGFloat(0)
        var bottomInset = CGFloat(0)

        if #available(iOS 11, *), enableInsets {
            let insets = self.safeAreaInsets
            topInset = insets.top
            bottomInset = insets.bottom
        }

        translatesAutoresizingMaskIntoConstraints = false

        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop+topInset).isActive = true
        }
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom-bottomInset).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
    }
}

extension UIImageView {
    @objc func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            } else {
                self?.image = UIImage(named: "emptyArtist")
            }
        }
    }
}
