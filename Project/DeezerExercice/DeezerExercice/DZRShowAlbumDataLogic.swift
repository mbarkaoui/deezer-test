//
//  DZRShowAlbumDataLogic.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol DZRShowAlbumDataLogicProtocol {
    func getAlbums(urlString: String, completion: @escaping (_ result: Result<AlbumViewModel?, Error>)->())
}

final class DZRShowAlbumDataLogic: DZRShowAlbumDataLogicProtocol {

    private let albumService: AlbumsAPIType
    private let trackService: TracksAPIType
    private let albumMapper: DZRAlbumMapperType
    private let tracksMapper: DZRTrackMapperType


    init(albumService: AlbumsAPIType = AlbumsAPI(), trackService: TracksAPIType = TracksAPI(), albumMapper: DZRAlbumMapperType = DZRAlbumMapper() , tracksMapper: DZRTrackMapperType =  DZRTrackMapper()) {
        self.albumService = albumService
        self.trackService = trackService
        self.albumMapper = albumMapper
        self.tracksMapper = tracksMapper
    }

    func getAlbums(urlString: String, completion: @escaping (Result<AlbumViewModel?, Error>) -> ()) {

        var albumViewModel: AlbumViewModel?

        let semaphore = DispatchSemaphore(value: 1)

        DispatchQueue.global().async {
            semaphore.wait()
            self.albumService.getAlbums(urlString: urlString) { response in
                switch response {
                case .success(let albumsResponse):
                    guard let albumData = albumsResponse.albums, !albumData.isEmpty else {
                        completion(.success(nil))
                        return
                    }

                    albumViewModel = self.albumMapper.map(album: albumData[0])
                    semaphore.signal()
                case.failure(let error):
                    completion(.failure(error))
                }
            }
        }

        DispatchQueue.global().async {
            semaphore.wait()
            guard let albumData = albumViewModel else {
                completion(.success(nil))
                return
            }

            self.trackService.getTracks(urlString: albumData.trackList) { response in
                switch response {
                case .success(let tracksResponse):
                    let albumTracks = self.tracksMapper.map(restTracks: tracksResponse.tracks)
                    albumData.albumTracks = albumTracks
                    completion(.success(albumViewModel))
                    semaphore.signal()
                case.failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
