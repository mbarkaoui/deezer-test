//
//  DZRSearchArtistsMapper.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol DZRSearchArtistsMapperType {
    func map(restArtists: [RestArtist]) -> [ArtistViewModel]
}

final class DZRSearchArtistsMapper: DZRSearchArtistsMapperType {
    func map(restArtists: [RestArtist]) -> [ArtistViewModel] {
        return restArtists.map(map)
    }

    private func map(restArtist: RestArtist) -> ArtistViewModel {

        return ArtistViewModel(artistName: restArtist.name ?? "", artistImage: restArtist.picture ?? "", trackList: restArtist.tracklist ?? "")
    }
}
