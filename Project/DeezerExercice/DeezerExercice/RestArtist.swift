//
//  RestArtist.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public struct RestArtist {
    public let id: Int?
    public let name: String?
    public let picture: String?
    public let pictureSmall: String?
    public let pictureMedium: String?
    public let pictureBig: String?
    public let pictureXl: String?
    public let nbAlbum: Int?
    public let nbFan: Int?
    public let radio: Bool?
    public let tracklist: String?
    public let type: String?
}

extension RestArtist: Decodable {
    enum UserCodingKeys: String, CodingKey {
        case id
        case name
        case picture
        case pictureSmall = "picture_small"
        case pictureMedium = "picture_medium"
        case pictureBig = "picture_big"
        case pictureXl = "picture_xl"
        case nbAlbum = "nb_album"
        case nbFan = "nb_fan"
        case radio
        case tracklist
        case type
    }

    public init(from decoder: Decoder) throws {
        let userContainer = try decoder.container(keyedBy: UserCodingKeys.self)

        id = try userContainer.decode(Int.self, forKey: .id)
        name = try userContainer.decode(String.self, forKey: .name)
        picture = try userContainer.decode(String.self, forKey: .picture)
        pictureSmall = try userContainer.decode(String.self, forKey: .pictureSmall)
        pictureMedium =  try userContainer.decode(String.self, forKey: .pictureMedium)
        pictureBig =  try userContainer.decode(String.self, forKey: .pictureBig)
        pictureXl =  try userContainer.decode(String.self, forKey: .pictureXl)
        nbAlbum =  try userContainer.decode(Int.self, forKey: .nbAlbum)
        nbFan =  try userContainer.decode(Int.self, forKey: .nbFan)
        radio =  try userContainer.decode(Bool.self, forKey: .radio)
        tracklist =  try userContainer.decode(String.self, forKey: .tracklist)
        type =  try userContainer.decode(String.self, forKey: .type)
    }
}
