//
//  Router.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

protocol NetworkRouter: AnyObject {
    associatedtype EndPoint: EndPointType
    func request<T:Decodable>(_ route: EndPoint, completion: @escaping (Result<T,Error>) -> ())
    func cancel()
}

class Router<EndPoint: EndPointType>: NetworkRouter {

    func request<T:Decodable>(_ route: EndPoint, completion: @escaping(_ data: Result<T,Error>) -> ()) {
        let session = URLSession.shared
        do {
            let request = try self.buildRequest(from: route)
            NetworkLogger.log(request: request)
            task = session.dataTask(with: request, completionHandler: { data, response, error in

                if error != nil {
                    completion(.failure(NSError(domain: "network request failed", code: 20, userInfo: nil)))
                }

                if let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200...299:
                        guard let responseData = data else {
                            completion(.failure(NSError(domain: "noData", code: 20, userInfo: nil)))
                            return
                        }
                        do {
                            _ = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                            let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
                            completion(.success(apiResponse))
                        }catch {
                            completion(.failure(NSError(domain: "We could not decode the response", code: 21, userInfo: nil)))
                        }
                    case 401...500:
                        completion(.failure(NSError(domain: "authentication error", code: 500, userInfo: nil)))
                    case 501...599:
                        completion(.failure(NSError(domain: "badRequest", code: 599, userInfo: nil)))
                    case 600:
                        completion(.failure(NSError(domain: "The url you requested is outdated", code: 599, userInfo: nil)))
                    default:
                        completion(.failure(NSError(domain: "Network request failed", code: 0, userInfo: nil)))
                    }
                }
            })
        }catch {
        }
        self.task?.resume()
    }

    private var task: URLSessionTask?

    func cancel() {
        self.task?.cancel()
    }

    fileprivate func buildRequest(from route: EndPoint) throws -> URLRequest {

        var completeURLPath: URL?
        if route.baseURL.isEmpty {
            completeURLPath = URL(string: route.path)
        } else {
            completeURLPath = URL(string: route.baseURL)?.appendingPathComponent(route.path)
        }
        var request = URLRequest(url: completeURLPath!,
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)

        request.httpMethod = route.httpMethod.rawValue
        do {
            switch route.task {
            case .request:

                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case .requestParameters(let bodyParameters,
                                    let bodyEncoding,
                                    let urlParameters):

                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)

            case .requestParametersAndHeaders(let bodyParameters,
                                              let bodyEncoding,
                                              let urlParameters,
                                              let additionalHeaders):

                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
            }
            return request
        } catch {
            throw error
        }
    }

    fileprivate func configureParameters(bodyParameters: Parameters?,
                                         bodyEncoding: ParameterEncoding,
                                         urlParameters: Parameters?,
                                         request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request,
                                    bodyParameters: bodyParameters, urlParameters: urlParameters)
        } catch {
            throw error
        }
    }

    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }

}

