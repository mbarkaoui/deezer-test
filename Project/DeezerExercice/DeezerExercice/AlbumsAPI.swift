//
//  AlbumsAPI.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public protocol AlbumsAPIType {
    func getAlbums(urlString: String, completion: @escaping (_ result: Result<AlbumsResponse, Error>)->())
}

final class AlbumsAPI: AlbumsAPIType {

    let albumsRouter = Router<AlbumsApi>()

    public func getAlbums(urlString: String, completion: @escaping (_ result:Result<AlbumsResponse, Error>)->()){

        albumsRouter.request(.albums(urlString)) { (result: Result<AlbumsResponse, Error>) in
            completion(result)
        }
    }
}
