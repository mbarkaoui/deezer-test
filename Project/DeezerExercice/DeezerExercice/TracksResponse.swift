//
//  TracksResponse.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public struct TracksResponse {
    let tracks: [RestTrack]
    let total: Int
}

extension TracksResponse: Decodable {

    private enum TracksResponseKeys: String, CodingKey {
        case tracks = "data"
        case total
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: TracksResponseKeys.self)
        tracks = try container.decode([RestTrack].self, forKey: .tracks)
        total = try container.decode(Int.self, forKey: .total)
    }
}
