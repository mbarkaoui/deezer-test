//
//  SearchArtistsDataLogicTests.swift
//  DeezerExerciceTests
//
//  Created by Malek BARKAOUI on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import XCTest
@testable import DeezerExercice

class SearchArtistsDataLogicTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func testWhenGetArtistsCalled() {
        //Given
        let artistApiMock = ArtistAPIMock.mock(result: .success(
                                                ArtistsResponse(
                                                    artists: [
                                                        RestArtist.mock(id: 1, name: "artist1", picture: "picture1"),
                                                        RestArtist.mock(id: 2, name: "artist2", picture: "picture2"),
                                                        RestArtist.mock(id: 3, name: "artist3", picture: "picture3")
                                                    ],
                                                    total: 3,
                                                    next: "")))

        let searchDataLogic = DZRSearchArtistsDataLogic(service: artistApiMock)

        let expectation = self.expectation(description: "artistsFetched")

        //When
        searchDataLogic.getArtists(artistName: "") { result in
            switch result {
            case .success(let artistList):
                //Then
                XCTAssertEqual(artistList.count, 3)
                XCTAssertEqual(artistList.first?.artistName, "artist1")
                XCTAssertEqual(artistList.first?.artistImage, "picture1")

                expectation.fulfill()
            case .failure(_):
                XCTFail()
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

}

extension RestArtist {
    static func mock(id: Int, name: String, picture: String, trackList: String = "") -> RestArtist{
        return RestArtist(id: id, name: name, picture: picture, pictureSmall: nil, pictureMedium: nil, pictureBig: nil, pictureXl: nil, nbAlbum: nil, nbFan: nil, radio: nil, tracklist: trackList, type: nil)
    }
}
