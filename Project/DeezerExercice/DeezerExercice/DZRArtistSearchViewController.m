//
//  DZRArtistSearchViewController.m
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import "DZRArtistSearchViewController.h"
#import "DZRArtistCollectionViewCell.h"
#import "DeezerExercice-Swift.h"


@interface DZRArtistSearchViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic) NSArray *artists;

@end

@implementation DZRArtistSearchViewController

- (void)viewDidLoad
{
    _searchBar.showsCancelButton = false;
    self.title = @"Artists";
    self.viewModel = [DZRSearchArtistsViewModel builder];
    __weak typeof(self) weakSelf = self;

    self.viewModel.onError = ^(NSError * error){
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf presentAlertWithAlertMessage: @"Oups something went wrong" completion:^{
                [weakSelf dismissAlert];
            }];
        });
    };

    self.viewModel.onArtistsListFetched = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
        });
    };

    self.viewModel.onArtistSelected = ^(ArtistViewModel * artistViewModel){
        dispatch_async(dispatch_get_main_queue(), ^{
            UIViewController *vc = [DZRShowAlbumViewController builderWithTrackList: artistViewModel.trackList];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        });
    };
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - Search

- (void)searchArtistsWithName:(NSString *)name {
    [self.viewModel getArtistsWithArtistName:name];
}

#pragma - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _searchBar.showsCancelButton = true;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(searchArtistsWithName:) object:nil];
    [self performSelector:@selector(searchArtistsWithName:) withObject:searchText afterDelay:0.5];

}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    _searchBar.showsCancelButton = false;
}


#pragma - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.viewModel artistListCount];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ArtistCollectionViewCellIdentifier";

    DZRArtistCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: CellIdentifier forIndexPath: indexPath];
    ArtistViewModel *artisteViewModel = [self.viewModel artistForRow: indexPath.row];
    [cell configure: artisteViewModel];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.viewModel didSelectArtistWithIndex: indexPath.row];
}

@end
