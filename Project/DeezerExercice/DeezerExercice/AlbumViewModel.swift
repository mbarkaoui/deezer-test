//
//  AlbumViewModel.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class AlbumViewModel {

    let albumName: String
    let albumImage: String
    let trackList: String
    var albumTracks: [TrackViewModel] = []
    
    init(albumName: String, albumImage: String, trackList: String) {
        self.albumName = albumName
        self.albumImage = albumImage
        self.trackList = trackList
    }
}
