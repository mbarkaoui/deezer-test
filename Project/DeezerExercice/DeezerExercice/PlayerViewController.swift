//
//  PlayerViewController.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 05/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController {
    private let holder = UIView()

    private let trackNameLabel = UILabel()
    private let albumNameLabel = UILabel()
    private let artistNameLabel = UILabel()
    private let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    private let playPauseButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    private let nextButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    private let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    private let verticalStackView = UIStackView()
    private let horizontalStackView = UIStackView()
    private let albumImageView = UIImageView()
    private let slider = UISlider()
    private let viewModel: PlayerViewModel

    var safeArea: UILayoutGuide!

    required init(viewModel: PlayerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        view.addSubview(holder)
        view.addSubview(closeButton)
        closeButton.setImage(UIImage(named: "closeIcon"), for: .normal)
        closeButton.anchor(top: view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 15, width: 30, height: 30, enableInsets: false)
        closeButton.addTarget(self, action: #selector(didTapOnCloseButton), for: .touchUpInside)

        holder.addSubview(albumImageView)


        //setup holder
        holder.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)

        //setup albumImageView
        albumImageView.layer.cornerRadius = 10
        albumImageView.clipsToBounds = true
        albumImageView.anchor(top: holder.topAnchor, left: holder.leftAnchor, bottom: nil, right: holder.rightAnchor, paddingTop: 50, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, width: 0, height: 300, enableInsets: false)

        //setup verticalStackView
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fillEqually
        verticalStackView.spacing = 20

        holder.addSubview(verticalStackView)
        verticalStackView.anchor(top: albumImageView.bottomAnchor, left: holder.leftAnchor, bottom: nil, right: holder.rightAnchor, paddingTop: 30, paddingLeft: 10, paddingBottom: 10, paddingRight: 0, width: 0, height: 0, enableInsets: false)

        
        trackNameLabel.textAlignment = .center
        albumNameLabel.textAlignment = .center
        artistNameLabel.textAlignment = .center

        verticalStackView.addArrangedSubview(trackNameLabel)
        verticalStackView.addArrangedSubview(albumNameLabel)

        //setup slider
        holder.addSubview(slider)
        slider.anchor(top: verticalStackView.bottomAnchor, left: holder.leftAnchor, bottom: nil, right: holder.rightAnchor, paddingTop: 0, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 40, enableInsets: false)

        slider.addTarget(self, action: #selector(didSlideSlider(_:)) , for: .valueChanged)

        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fillEqually
        horizontalStackView.spacing = 30

        holder.addSubview(horizontalStackView)

        horizontalStackView.centerXAnchor.constraint(equalTo: holder.centerXAnchor).isActive = true

        horizontalStackView.anchor(top: slider.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 30, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        playPauseButton.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 50, height: 50, enableInsets: false)
        backButton.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 50, height: 50, enableInsets: false)
        nextButton.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 50, height: 50, enableInsets: false)
        playPauseButton.setBackgroundImage(UIImage(named: "pause"), for: .normal)
        nextButton.setBackgroundImage(UIImage(named: "next"), for: .normal)
        backButton.setBackgroundImage(UIImage(named: "back"), for: .normal)


        playPauseButton.addTarget(self, action: #selector(didTapOnPlayPauseButton), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(didTapOnNextButton), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(didTapOnBackButton), for: .touchUpInside)

        horizontalStackView.addArrangedSubview(backButton)
        horizontalStackView.addArrangedSubview(playPauseButton)
        horizontalStackView.addArrangedSubview(nextButton)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        configure()
        viewModel.viewDidLoad()
    }

    private func bind() {

        viewModel.updateSliderLevel = { [weak self] time, duration in
            Runner.runOnMainThread {
                self?.slider.value = Float(time)/Float(duration)
            }
        }

        viewModel.onViewDidLoad = { [weak self] track in
            Runner.runOnMainThread {
                self?.viewModel.audioPlayer.playPauseAudio(track: track.trackPreview)
            }
        }

        viewModel.trackDidplaytoEnd = { [weak self] in
            Runner.runOnMainThread {
                UIView.animate(withDuration: 0.6, animations: {
                    self?.albumImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                })

                self?.slider.value = 0
                self?.playPauseButton.setBackgroundImage(UIImage(named: "play"), for: .normal)
            }
        }

        viewModel.playPauseTrack = { [weak self] isPlaying in
            Runner.runOnMainThread {

                if isPlaying {
                    UIView.animate(withDuration: 0.6, animations: {
                        self?.albumImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                    })

                    self?.playPauseButton.setBackgroundImage(UIImage(named: "play"), for: .normal)
                } else {
                    UIView.animate(withDuration: 0.6, animations: {

                        self?.albumImageView.transform = .identity
                    })
                    self?.playPauseButton.setBackgroundImage(UIImage(named: "pause"), for: .normal)

                }
            }
        }

        viewModel.playNextTrack = { [weak self] track in
            Runner.runOnMainThread {

                self?.albumImageView.load(url: URL(string: self?.viewModel.getAlbumName() ?? "") ?? URL(fileURLWithPath: ""))
                self?.trackNameLabel.text = track.trackName
                self?.albumNameLabel.text = self?.viewModel.albumViewModel.albumName
            }
        }


        viewModel.playPreviousTrack = { [weak self] track in
            Runner.runOnMainThread {

                self?.albumImageView.load(url: URL(string: self?.viewModel.getAlbumName() ?? "") ?? URL(fileURLWithPath: ""))
                self?.trackNameLabel.text = track.trackName
                self?.albumNameLabel.text = self?.viewModel.albumViewModel.albumName
            }
        }
    }

    private func configure() {
        self.view.backgroundColor = .white

        albumImageView.load(url: URL(string: self.viewModel.albumViewModel.albumImage) ?? URL(fileURLWithPath: ""))
        trackNameLabel.text = self.viewModel.albumViewModel.albumTracks[viewModel.index].trackName
        albumNameLabel.text = self.viewModel.albumViewModel.albumName

    }
    @objc func didTapOnCloseButton(_ button: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }


    @objc func didTapOnPlayPauseButton(_ button: UIButton) {
        viewModel.didTapPlayPause()
    }

    @objc func didTapOnNextButton(_ button: UIButton) {
        viewModel.didTapNextTrack()
    }

    @objc func didTapOnBackButton(_ button: UIButton) {
        viewModel.didTapBackTrack()
    }

    @objc func didSlideSlider(_ slider: UISlider) {

        guard let itemPlayedDuration = viewModel.audioPlayer.player?.currentItem?.asset.duration.seconds else {
            return
        }
        let playerTimescale = viewModel.audioPlayer.player?.currentItem?.asset.duration.timescale ?? 1
        let time =  CMTime(seconds: Double(itemPlayedDuration) * Double(slider.value), preferredTimescale: playerTimescale)

        viewModel.audioPlayer.player?.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)

    }

    override func viewWillDisappear(_ animated: Bool) {
        viewModel.audioPlayer.stop()
    }
}



extension PlayerViewController {
    static func builder(viewModel: PlayerViewModel) -> PlayerViewController {
        return PlayerViewController(viewModel: viewModel)
    }
}

