//
//  ArtistsAPI.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public protocol ArtistsAPIType {
    func getArtists(name: String, completion: @escaping (_ result: Result<ArtistsResponse, Error>)->())
}

final class ArtistsAPI: ArtistsAPIType {
    
    let artistRouter = Router<ArtistApi>()
    
    public func getArtists(name: String, completion: @escaping (_ result:Result<ArtistsResponse, Error>)->()){
        
        artistRouter.request(.artists(name)) { (result: Result<ArtistsResponse, Error>) in
            completion(result)
        }
    }
}
