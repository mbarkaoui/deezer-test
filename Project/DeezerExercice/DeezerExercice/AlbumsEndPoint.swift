//
//  AlbumsEndPoint.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 03/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public enum AlbumsApi {
    case albums(String)
}

extension AlbumsApi: EndPointType {

    var environmentBaseURL: String {
        switch NetworkManager.environment {
        case .production: return ""
        case .qa: return ""
        case .staging: return ""
        }
    }

    var baseURL: String {
        let url = environmentBaseURL
        return url
    }

    var path: String {
        switch self {
        case .albums(let urlString):
            return urlString
        }
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var task: HTTPTask {
        switch self {
        case .albums:
            return .request
        }
    }

    var headers: HTTPHeaders? {
        return nil
    }
}
