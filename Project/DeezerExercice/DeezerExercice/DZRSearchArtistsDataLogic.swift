//
//  DZRSearchArtistsDataLogic.swift
//  DeezerExercice
//
//  Created by Malek BARKAOUI on 02/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public protocol DZRSearchArtistsDataLogicProtocol {
    func getArtists(artistName: String, completion: @escaping (_ result: Result<[ArtistViewModel], Error>)->())
}

final class DZRSearchArtistsDataLogic: DZRSearchArtistsDataLogicProtocol {
    
    private let service: ArtistsAPIType
    private let mapper: DZRSearchArtistsMapperType
    
    init(service: ArtistsAPIType = ArtistsAPI(), mapper: DZRSearchArtistsMapperType = DZRSearchArtistsMapper()) {
        self.service = service
        self.mapper = mapper
    }
    
    func getArtists(artistName: String, completion: @escaping (Result<[ArtistViewModel], Error>) -> ()) {

        service.getArtists(name: artistName) { response in
            switch response {
            case .success(let artistResponse):
                completion(.success(self.mapper.map(restArtists: artistResponse.artists)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
